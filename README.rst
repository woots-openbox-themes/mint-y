==============
Openbox Mint-Y
==============

An Openbox theme matching the Mint-Y GTK theme.

* GTK theme: Mint-Y
* Icons: Mint-X
* Font: Cantarell
* Programs: Conky system monitor, PCManFM, Sakura terminal

.. image:: doc-screenshot.png
   :width: 892
   :target: screenshot.png
